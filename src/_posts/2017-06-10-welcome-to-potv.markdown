---
layout: post
title:  "Welcome to People on TV!"
date:   2017-06-10 01:53
categories: blog
---

Welcome to People on TV y'all! There's not much to see here at
the moment, but we're hard at work in the podcast mines digging
out and harvesting that reality TV content that you crave. Stay
tuned for more updates about our burgeoning podcast!!
