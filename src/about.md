---
layout: page
title: About
permalink: /about/
---

People On TV is made up of [Will](https://twitter.com/worr),
[Logan](https://twitter.com/explogan42) and
[Tia](https://twitter.com/dalek_party)! Every two weeks, we're
excited to share the latest reality TV that we've suffered through
for your enjoyment.


You can find us at [our Twitter page](https://twitter.com/PeopleOnTVCast)
or by [emailing us](mailto:thepeople@peopleon.tv). Feel free to
send us recommendations! We're always on the hunt for a new tire fire.
