const path = require("path"),
    webpack = require("webpack");
module.exports = {
    entry: path.resolve(__dirname, "src/ts/index.ts"),
    output: {
        filename: "./src/assets/bundle.js"
    },
    devtool: "source-map",
    module: {
        rules: [{
                test: /\.ts$/,
                loader: 'ts-loader'
            }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true
        })
    ]
};
